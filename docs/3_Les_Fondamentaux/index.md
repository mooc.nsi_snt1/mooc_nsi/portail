---
title: Présentation
description: "Présentation du MOOC: Les Fondamentaux du numérique et des sciences informatiques"
---

## La formation : Apprendre les fondamentaux nécessaires pour ensuite enseigner au niveau du secondaire supérieur.

[![](../assets/vignette-NSI1-sc-600px.png)](https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/)

<b style="display:block;float:right">[S'inscrire au MOOC](https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/)</b>

### Objectifs pédagogiques

L'objectif d'acquérir les bases théoriques et une première expérience pratique dans tous les champs de l'informatique (programmation, bases de données, algorithmie, architecture, réseau et systèmes d'exploitation) pour envisager l'enseignement de l'informatique au niveau du secondaire supérieur. En France, se préparer à enseigner au lycée avec le passage du CAPES Informatique.

On permet donc à des enseignant·e·s ou en passe de l'être de se doter eux-mêmes des compétences en informatique nécessaires, y compris prendre de la hauteur et du recul par rapport à la matière enseignée.

### Format de la formation

C'est une formation "massive" qui va au delà de ce qui se fait usuellement dans un MOOC puisque  nous offrons un parcours qui couvre l'ensemble du programme d'informatique enseigné au lycée en spécialité, y compris les notions en amont pour partager les savoirs et faire acquérir les savoir faire attendus, avec des exercices d'auto-évaluation de type quiz et autres, et des activités utilisant des outils numériques divers.

### Prérequis pour l'apprenant·e

Avoir une culture scientifique et technique de base en informatique et être déjà initié à la programmation python, tel que explicité ci-dessous où on donne les liens vers les formations disponibles pour acquérir ces compétences.

### Compétences acquises et évaluation

Les compétences actives sont celles exprimées dans les objectifs pédagogiques.

On peut citer par exemple, mettre en oeuvre les algorihmes classiques de l'informatique, maîtriser et utiliser les structures de données disponibles, comprendre et configurer les équipements d'un réseau local, etc.

L'évaluation se fera en auto-évaluation par des exercices en ligne et hors ligne, évaluées sur la plateforme.

## Vos questions!

### Quelles sont les formations préliminaires à cette formation "lourde" ?

Il faut connaître les concepts de base et s’initier à la programmation. En bref on invite à [s'initier à l'enseignement en Sciences Numériques et Technologie (SNT)](https://www.fun-mooc.fr/fr/cours/sinitier-a-lenseignement-en-sciences-numeriques-et-technologie) et s'initier à la programmation Python. Il est aussi recommander d'approfondir sa culture scientifique du numérique.

#### Acquérir une culture scientifique et technique de base en informatique

- En allant https://classcode.fr/snt => _section S_ vous aurez les premières bases, le vocabulaire, les idées introductives, un moyen aussi de vérifier que cela est fait pour vous ;)

- https://classcode.fr/snt => _section S+_ permet d'aborder des grands sujets du numériques à la fois techniques et sociétaux pour que les choses fassent du sens au delà de la formation disciplinaire à l'informatique.

- https://classcode.fr/snt => _section N_ correspond aux thématiques de l'enseignement des _sciences numériques et technologie_ [SNT](https://fr.wikipedia.org/wiki/Sciences_num%C3%A9riques_et_technologie), et permet de mieux connaître ces sujets.

Note : Il faudra aussi disposer de ressources sur les métiers et secteurs industriels du numérique, c'est en cours.

Note : There is also an [English version of these resources](https://pixees.fr/classcode-v2/classcode-informatics-and-digital-creation-online-free-open-course/) if of any help, including digital culture skills acquisition.

#### S’initier à la programmation Python :

Nous proposons trois solutions possibles, selon qu'on enseigne au niveau élémentaire de découverte de la programmation ou à un niveau plus avancé :

- Introduction minimale pour SNT : 
    - https://classcode.fr/snt => _section T_ sélectionne les bases pour commencer à programmer.
- Formation minimale pour NSI : 
    - [Apprendre à coder avec Python](https://www.fun-mooc.fr/courses/course-v1:ulb+44013+session04/about)
- Formation avancée pour NSI : 
    - [Python 3 : des fondamentaux aux concepts avancés du langage](https://www.fun-mooc.fr/courses/course-v1:UCA+107001+session02/about)

Bien entendu ce n'est pas exclusif, toute bonne formation à la programmation Python sera parfaite.

### Quelles formations complémentaires sont indiquées ?

#### Connaître un peu l'histoire de l'informatique

On découvrira aussi avec bénéfice des éléments de l'histoire de l’informatique que nous proposons ici sous forme de deux ressources réutilisables avec les élèves: 

- https://project.inria.fr/classcode/profiter-de-classcode-en-postcast sous forme de podcats qui permettent de décourvriri des femmes et des hommes qui ont fait l'histoire de l'informatique

- http://sparticipatives.gforge.inria.fr/film qui présente ces contenus sous forme d'un livret et d'une web série.

#### Intelligence artificielle

- https://classcode.fr/iai propose une formation citoyenne gratuite et attestée, 
    - qui correspond à la formation en IA qui se fait en France au niveau de l'enseignement des sciences en dernière année du secondaire.
    - et est proposée dans le cadre d’une perspective [« d’Université Citoyenne et Populaire en Sciences et Culture du Numérique »](https://hal.inria.fr/hal-02145480) où chacune et chacun de la chercheuse au politique en passant par l’ingénieure ou l’étudiant venons avec nos questionnements, nos savoirs et savoir-faire à partager.
- https://www.elementsofai.fr est une formation technique à l'IA, ``de niveau NSI´´ pour les enseignant·e·s ou les élèves qui voudraient aller plus loin sur ces sujets, grâce à la formation NSI.

On peut avoir une discussion sur ces formation [ici](https://www.lemonde.fr/blog/binaire/2021/01/12/formation-a-lintelligence-artificielle-la-bande-annonce/)

#### Imapct environnementaux du numérique 

Il est essentiel quand on se forme à l'informatique de prendre conscience des enjeux environnementaux du numérique, pour les comprendre et gérer les choses au mieux à ce niveau. 

Impact Num est un MOOC pour se questionner sur les impacts environnementaux du numérique, apprendre à mesurer, décrypter et agir, pour trouver sa place de citoyen dans un monde numérique. Il aborde l’impact du numérique sur l’environnement, ses effets positifs et négatifs, les phénomènes observables aujourd’hui et les projections que nous sommes en mesure de faire pour l’avenir.

https://www.fun-mooc.fr/en/cours/impacts-environnementaux-du-numerique

C'est une facette indispensable de la formation à l'informatique au 21ème siècle.

#### Autres compléments

- Pour qui n'a jamais programmé, un cours d'initiation à Scratch, tel que pratiqué au collège et en primaire est disponible : https://project.inria.fr/classcode/initiation-a-scratch-en-autonomie 

- La robotique pédagogique est aussi un puissant le levier et [Le robot Thymio comme outil de découverte des sciences du numérique](https://www.fun-mooc.fr/courses/course-v1:inria+41017+session01/about) offre une formation de choix sur ces sujets.

<script src="../assets/make_foldable.js"/>

