---
title: Vous aider plus …
description: La communauté des SNT et NSI ``cookies´´ cuisiner ensemble ce dont nous avons besoin pour apprendre et enseigner SNT et NSI.
hide:
- navigation
---
<table>
<tbody>
<tr>
<td><br/><br/><br/><a href="http://terra-numerica.org/"><img class="aligncenter" src="https://terra-numerica.org/files/2020/10/logo-TN-transp.png" alt="Terra Numerica" width="256" height="65" /></a></td>
<td><img class="aligncenter" src="https://gitlab.com/mooc-nsi-snt/portail/-/raw/master/assets/logo-nsi-cookies.png" alt="funny cooky'faces" width="204"  /></td>
<td><br/><br/><br/><a href="https://learninglab.inria.fr/"><img class="aligncenter" src="https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2022/11/logo-learninglab-inria.png" alt="Learning Lab Inria" width="187" height="67" /></a></td>
<td><br/><br/><a href="https://aeif.fr/"><img class="aligncenter" src="https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2022/11/logo-aeif.png" alt="Learning Lab Inria" /></a></td>
</tr>
</tbody>
</table>

<p style="float:right"><h2><a href="https://mooc-nsi-snt.gitlab.io/portail/NSI-cookies/2023_programme.html">Demandez le programme :)</a></h2></p>

# La communauté des SNT et NSI ``cookies´´ : cuisiner ensemble ce dont nous avons besoin pour apprendre et enseigner SNT et NSI. 

Nous, Terra Numerica et ses partenaires, partageons une offre d'accompagnement tutorée hybride:

- Je suis prof en lycée et on vient de m'attribuer des enseignements SNT … comment vais-je pouvoir m'y lancer ?
- J'enseigne la spécialité NSI et je partegerais bien mes pratiques, surtout par rapport aux élèves en difficulté ?
- Je fais beaucoup d'informatique, comment en faire mon métier en tant qu'enseignant·e ?
- Je n'ai pas eu la chance d'avoir des cours d'informatique au lycée, puis-je tout de même profiter de ce qu'on y enseigne maintenant ?

Nous proposons des rencontres en présentiel en PACA-est et aussi des rencontres en ligne.__

Et vous, quel est votre besoin ? Quels thèmes voudriez vous aborder? Êtes vous partant·e pour une rencontre ? Ce [questionnaire de 5 minutes](https://sondages.inria.fr/index.php/812311?newtest=Y&lang=fr) va nous aider à vous aider !

Oui : rejoignez nous, nous allons vous aider et vous accompagner (gratuitement) !

Pour [discuter avec nous](https://mooc-forums.inria.fr/moocnsi/c/faire-communautc3a9/nsi-cookies/224) c'est pas ici, pour nous poser une question par c'est par là: [nsi-cookies@terra-numerica.org](mailto:nsi-cookies@terra-numerica.org).

## Vos Questions !

### C'est pour qui et pour quels besoins ?

- _aux enseignant·e·s_, en SNT et NSI qui doivent s’initier à l'informatique (en SNT en Seconde, spécialité NSI en 1ere et Terminale, au collège et en primaire) de se former (formation initiale ou complémentaire), en lien avec l'INSPÉ de l'académie de Nice et le rectorat;

- _aux professionnel·le·s de l'informatique_, hors éducation nationale, qui souhaitent se réorienter vers l’enseignement, et on besoin à la fois d'élargir ou mettre à jour leurs connaissances et savoir-faire en informatique au delà de leurs hautes compétences dans un domaine particulier et d'apprendre à enseigner;

- _aux personnes de tout niveau_ et toute branche professionnelle qui veulent augmenter leurs compétences en informatique pour mieux interagir avec leurs partenaires du monde de l'informatique.
  - nous ciblerons tout particulièrement les personnes venant d'obtenir un diplôme académique (par exemple dans le cadre d’une démarche de VAE https://www.vae.gouv.fr, inscription aux formations pour adulte, etc.) en leur offrant une remise à niveau préalable et un accompagnement dans leur démarche de formation.

### Concrètement vous faites quoi en fait ?

- Nous vous invitons sur le [forum de partage et d'entraide](https://mooc-forums.inria.fr/moocnsi/) pour venir y partager vos questions, vos idées, vos témoignages, bref faire communauté.
- Nous avons un point de contact cai-contact@inria.fr où vous pouvez poser votre question "personnell" quelle qu'elle soit. 
- Au fil des formations que nous proposons, nous écoutons vos retours, vos suggestions, et vous aidons.

- Sur un des territoire en PACA est nous organisons des rencontres en présentiel sur une thématique ou un questionnement précis pour avancer en profondeur sur le sujet, nous ouvrons ces rencontres aussi en ligne au niveau francophone.

- Des collègues chercheur·e·s en science de l'éducation nous conseillent, partagent leur expertise et contribuent à évaluer ce que nous offrons.

Et … nous sommes ouverts à d'autres idées.

### Où se trouvent les rencontres présentielles sur le territoire ?

Les formations se font sur les différent [Espaces Partenaires Terra Numerica](https://terra-numerica.org/espaces-partenaires-terra-numerica/) (EPTN) du territoire "PACA est". Selon les publics et les besoins on pourra aussi aller dans un établissement scolaire ou un lieu d’accueil en lien avec ces sujets.

- _Sur place_, les personnes du territoire "PACA est":
  - autour de Sophia Antipolis avec [Terra Numerica@sophia](https://terra-numerica.org/terra-numerica-sophia/) (voir la [localisation](https://goo.gl/maps/pjKod1tTmrcNg93v6))
  - sur les sites de [l'INSPÉ](https://inspe.univ-cotedazur.fr/) Espaces Partenaires Terra Numerica.
    - près de Toulon sur [le site de la Seyne-sur-Mer](https://goo.gl/maps/XkBaWqWk8qLJJfqv8).
    - sur Nice (si besoin ) sur [le site de Liégeard](https://g.page/Inspe_Liegeard?share).

- _En ligne_ de manière synchrone extra-territoriale, le public francophone, de manière, à travers une offre en ligne 
   - complétée, si besoin, d'une semaine intensive (à l'instar de https://smartedtech.eu) où les personnes seront invitées à un court séjour (accueil au CIV http://www.civfrance.com voisin) pour ancrer le travail communautaire dans un contact présentiel (en projet).


### Comment profiter des webinaires ?

- Il suffit de regarder la page du [programme 2023](./NSI-cookies/2023_programme.html).

- Et tous les Webinaires sont disponibles en rediffusion
    -  [à l'intérieur du MOOC NSI](https://lms.fun-mooc.fr/courses/course-v1:inria+41027+session01/courseware/6884e60e54c0468f8141d9b29b439b4d/966980f217af4713887bdca3ea5c9250/).
    -  [sur Portail tubes de apps.education.fr](https://tube-numerique-educatif.apps.education.fr/c/cookies/videos).
    -  [sur la chaine youtube du MOOC NSI du Learning Lab](https://www.youtube.com/playlist?list=PLKGPGznq6a-Veb8AAaief6sm2qUGM12GE).

### Où aller discuter de tout ça ?

Le [forum](https://mooc-nsi-snt.gitlab.io/portail/5_Forum.html) c'est super, mais c'est … immense !!

Pour [discuter avec nous](https://mooc-forums.inria.fr/moocnsi/c/faire-communautc3a9/nsi-cookies/224) c'est pas ici, pour nous poser une question par c'est par là: [nsi-cookies@terra-numerica.org](mailto:nsi-cookies@terra-numerica.org).

Pour ce qui concerne l'apprentissages des fondamentaux c'est sur ce [fil de discussion](https://mooc-forums.inria.fr/moocnsi/t/nsi-cookies-vous-aider-plus-avec-et-au-dela-du-mooc-sur-les-fondamentaux/7680)

Pour ce qui concerne apprendre à enseigner c'est sur ce [fil de discussion](https://mooc-forums.inria.fr/moocnsi/t/nsi-cookies-vous-aider-plus-au-pendant-et-apres-le-mooc/7681).


### Comment commencer dès maintenant ?

Une formation de type https://classcode.fr/snt pour l'initiation à l'informatique et https://classcode.fr/iai pour la découverte de l'intelligence artificielle sont disponibles avec de l'accompagnement au sein de la formation.

Ensuite on pourra s'initier plus largement à Python, avec une partie de la formation https://classcode.fr/nsi pour une formation à plus haut niveau (par exemple une initiation à python) et aller plus loin avec https://www.elementsofai.fr pour une formation technique à l'intelligence artificielle. 

### Est-ce que mon travail sera attesté ?

Oui et à deux niveaux: 

- Au niveau des formations en ligne, la plateforme délivre des attestations de réussite qui peuvent être réutilisées au niveau d'un dossier, les collègues de l'Éducation Nationale connaissent ces formations et savent en tenir compte lors des évaluations (même si, pas d'erreur, ces attestations ne sont ni des diplômes, ni des certificatons)
- Au niveau de votre participation, sous l'égide de [Terra Numerica](http://terra-numerica.org/) qui est notre autorité de confiance, nous délivrons des lettres d'attestation qui décrivent le contenu de vos actions avec nous et leur réussite.

### Et tout ça c'est gratuit ?

Rien n'est gratuit ! Mais _oui_ tout ce que nous partageons, y compris les attestations de participation, est fait gratuitement, car nous sommes financés par des structures du service public comme [détaillé ici](https://mooc-nsi-snt.gitlab.io/portail/2_nous.html)

### Qui sont les partenaires ?

Porté par [Terra Numerica](https://terra-numerica.org) ce travail se fait :

- en partenariat avec le LearningLab Inria (https://learninglab.inria.fr) et son offre (https://learninglab.inria.fr/cours/) 
- et l'AEIF (https://aeif.fr/) qui anime la communauté,
- en lien avec Femmes et Sciences (https://www.femmesetsciences.fr) au niveau de l'égalité des genres dans l'enseignement de l'informatique qui est un priorité de cette action et aussi 
- en lien avec le champ d'action d'EducAzur (https://educazur.fr) et de la formation SmartEdTech (http://smartedtech.eu) avec une vision internationale et entrepreneuriale.

### Peut-on en savoir plus sur la démarche ?

Avec plaisir, notre [document de cadrage](https://docs.google.com/document/d/1NGXuWRAkTYj43dTWif7MgZTbFD_n7wrm_2t8PIT6uQk/edit) est accessible.

<script src="./assets/make_foldable.js"/>
