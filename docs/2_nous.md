---
title: Équipe et partenaires
description: 
hide:
- navigation
---

## Qui sommes nous ?

Nous sommes une [équipe](https://tinyl.io/41Ii) de professeures et professeurs d'informatique, d'enseignantes et enseignants chercheurs dans ce domaine, d'ingénieures et ingénieurs pédagogiques et développement. Nous nous rassemblons pour identifier les besoins, rassembler ou créer les ressources qui vont pouvoir aider les collègues et futurs collègues.

Nous travaillons en mode projet et co-animons forum et communauté. Vous allez nous croiser au fil des discussions et des ressources.

## Les acteurs

Le projet est porté et opéré par le [Learning Lab Inria](https://learninglab.inria.fr) avec la [Communauté d'Apprentissage de l'Informatique](https://cai.community) en appui :

<table>
<tr>
<td style="width:50%;margin:0 auto;vertical-align:middle"><img alt="Learning Lab Inria" src="./assets/logo-learninglab-inria.png" title="https://learninglab.inria.fr"/></td>
<td style="width:50%;margin:0 auto;vertical-align:middle"><img alt="Communauté d'Apprentissage de l'Informatique" src="https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2020/04/cropped-logo-cai-0-4.png" title="https://cai.community"/></td>
</tr>
</table>

en partenariat avec des collègues des établissements d'enseignement supérieur et de recherche ou associatif suivants :

<table>
<tr>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://ens-paris-saclay.fr"><img src="./assets/logo-ens-saclay.png" alt="ENS Paris Saclay" title="ENS Paris Saclay"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="http://www.ulb.ac.be"><img src="https://cai.community/wp-content/uploads/sites/3/2020/12/logo-300x101.png" alt="Université Libre de Bruxelles" title="Université Libre de Bruxelles"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.univ-reunion.fr"><img src="https://cai.community/wp-content/uploads/sites/3/2020/06/logo_ur.jpg" alt="Université de la Réunion" title="Université de la Réunion"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://univ-cotedazur.fr"><img src="https://cai.community/wp-content/uploads/sites/3/2020/06/logo_uca.jpg" alt="Université Côte d'Azur" title="Université Côte d'Azur"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="http://aeif.fr"><img src="./assets/logo-aeif.png" alt="Association des enseignantes et enseignants d'informatique de France" title="Association des enseignantes et enseignants d'informatique de France"></a></td>
</tr>
<tr>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.universite-paris-saclay.fr"><img src="https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2021/11/Universite-Saclay-NSI.png" alt="Université Paris Saclay" title="Université Paris Saclay"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.univ-grenoble-alpes.fr/"><img src="https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2021/11/Universite-Grenoble-Alpes-NSI.png" alt="Université Grenoble Alpes" title="Université Grenoble Alpes"></a></td>
<td style="width:20%;margin:0 auto;vertical…auto;vertical-align:middle"><a href="https://www.liglab.fr"><img src="https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2021/11/LIG-Laboratoire-d_Informatique-de-Grenoble-NSI.jpg" alt="Laboratoire d'Informatique de Grenoble" title="Laboratoire d'Informatique de Grenoble"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.umontpellier.fr/"><img src="https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2021/11/Logo_universite_montpellier-MOOC-NSI.png" alt="Université de Montpellier" title="Université de Montpellier"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.cyu.fr/"><img src="https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2022/01/Logo_Universite_Cergy_Pontoise.png" alt="Université de Cergy Pontoise" title="Université de Cergy Pontoise"></a></td>
</tr>
</table>

sous l'égide de la [Société Informatique de France](https://www.societe-informatique-de-france.fr) avec l'[EPI](https://www.epi.asso.fr/) et de l'association [Class´Code](https://classcode.fr) pour l'ancrage territorial avec pour le projet [NSI-cookies](https://mooc-nsi-snt.gitlab.io/portail/6_Communaute.html):

<table>
<tr>
<td style="width:50%;margin:0 auto;vertical-align:middle"><a href="http://terra-numerica.org/"><img width="300" src="http://terra-numerica.org/files/2020/10/cropped-logo-TN-transp.png" alt="Terra Numerica"></a></td>
</tr>
</table>

## Avec le soutien …

… d'[Inria](https://www.inria.fr) et de la [Direction du Numérique pour l’Éducation](https://www.education.gouv.fr/direction-du-numerique-pour-l-education-dne-9983), l'[Université Ouverte des Humanités](https://uoh.fr/) associée à l'[Université Numérique Ingénierie et Technologie](https://unit.eu) et celui d'[Inria](https://www.inria.fr) :

<table>
<tr>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.societe-informatique-de-france.fr"><img src="./assets/logo-sif.png" alt="Société Informatique de France"></a><br/><br/><br/><a href="https://www.epi.asso.fr/"><img src="./assets/logo-epi.png" alt="Société Informatique de France"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://classcode.fr"><img src="https://project.inria.fr/classcode/files/2016/07/LOGO-ClassCode-coul.jpg" alt="Class´Code"></a></td>
<td style="width:15%;margin:0 auto;vertical-align:middle"><a href="https://www.education.gouv.fr/direction-du-numerique-pour-l-education-dne-9983"><img src="./assets/logo-menj.png" alt="Direction du Numérique pour l’Éducation"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://unit.eu/"><img src="https://upload.wikimedia.org/wikipedia/commons/7/75/Logo_UNIT.svg" alt="Université Numérique Ingénierie et Technologie"></a><br/><br/><br/>
<a href="https://uoh.fr/"><img src="https://upload.wikimedia.org/wikipedia/fr/d/dc/Logo_UOH.svg" alt="Université Ouverte des Humanités"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.inria.fr"><img src="https://cai.community/wp-content/uploads/sites/3/2020/05/inr_logo_rouge-300x106.png" alt="Inria"></a></td>
</tr>
</table>

## Vos questions !

### Quels sont les documents de pilotage du projet ?

- En toute transparence voici nos documents de travail:
    - Le [document de référence](https://tinyl.io/416C) du projet et une [présentation préliminaire](https://tinyl.io/416B) fin 2020, qui décrit aussi la gouvernance, le budget, etc...
    - Un [inventaire des contenus prévus](https://tinyl.io/416D) et une [structuration provisoire](https://docs.google.com/document/d/1zrt_5cG8SqDLChbuk8RKY7_g19FWP82DsX3UBbuGE_E/edit) qui servent de base au travail en cours.

### Comment participer au projet lui-même ?

-  Bienvenue ! Il suffit de [nous contacter](./7_Contact.html), effectivement notre équipe grandit régulièrement et c'est tant mieux car la tâche est immense.</li>

- Participer officiellement à ce projet est aussi une belle reconnaissance pour ces collègues qui se sont investis à fond depuis des mois pour que cet enseignement réusisse : c'est un levier pour faire connaitre et partager ces contributions.</li>

<script src="./assets/make_foldable.js"/>
