---
title: Le Forum
description: Partager et s'entraîder sur le forum
hide:
- navigation
---

[![](./assets/logo-forum.png)](https://mooc-forums.inria.fr/moocnsi/)

Le [forum](https://mooc-forums.inria.fr/moocnsi/) est l'endroit où nous partageons, discutons et aussi nous rassemblons pour co-créer des contenus et des ressources.

https://mooc-forums.inria.fr/moocnsi/

Le but principal est de faciliter la recherche d’information : le découpage en catégories / sous-catégories reprenant les items du programme permettra aux utilisateurs actuels et futurs de retrouver rapidement les informations sur un thème donné.

Au-delà de la communauté des enseignants actuels de NSI, ce forum sera aussi celui de la formation en ligne NSI, dont les futurs candidats aux CAPES, en lien avec la Communauté d’Apprentissage de l’Informatique francophone.

Ce forum propose des fonctionnalités avancées (par exemple possibilité d’écrire du code en Markdown au sein d’un message, moteur de recherche performant, possibilité de clôturer un sujet en mettant en avance sa solution…) que nous vous laissons le plaisir de découvrir.

## Le tableau de bord du forum pour cette formation

<table>
<tr>
   <td><a href="https://mooc-forums.inria.fr/moocnsi/c/salon-de-discussion/mooc-1/198">FUN Mooc 1 : Les fondamentaux</a></td>
   <td>Pour les personnes participant à la formation <a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/index.html">Apprendre les fondamentaux</a> sur <a href="https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/">MOOC FUN</a>.</td>
   <td  valign="middle" rowspan="2">On y dialogue à propos des problèmes liés au fonctionnement de cette formation (ex: correction à remonter, aide à l’utilisation, attestation de la formation, …) et de ces enjeux (ex: quel investissement, quel aboutissement, quels compléments, …).</td>
</tr>
<tr>
   <td><a href="https://mooc-forums.inria.fr/moocnsi/t/a-propos-de-la-categorie-fun-mooc-la-pratique/3641">FUN Mooc 2 : La pratique</a></td>
   <td> Pour les personnes participant à la formation <a href="https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner/index.html">Apprendre à enseigner</a> sur <a href="https://www.fun-mooc.fr/fr/cours/apprendre-a-enseigner-le-numerique-et-les-sciences-informatiques/">MOOC FUN</a>.</td>
</tr>
<tr>
   <td><a href="https://mooc-forums.inria.fr/moocnsi/c/representation-des-donnees/a-propos-du-mooc/201">Bloc 1 du Mooc 1 : Représentation des données et de l’information</a></td>
   <td valign="middle" colspan="2" rowspan="4">On y dialogue  à la fois sur le déroulé (ex: besoin d’une explication de plus, d’une ressource complémentaire, …) et sur le fond du sujet, par exemple 

1. quelle représentations dans d’autres langages que Python, que se passe-t-il pour les huge data, … 
2. quels paradigmes de programmation en IA, quels liens entre langues humaines et langage formels, …
3. quoi apprendre en plus de ce que l’on va enseigner, quel lien avec la recherche en informatique, …
4. quelles architectures pour les objets connectés, quelles applications industrielles, …

</td>
</tr>
<tr>
   <td><a href="https://mooc-forums.inria.fr/moocnsi/c/langages-et-programmation/a-propos-du-mooc/202">Bloc 2 du Mooc 1 : Programmation</a></td>
</tr>
<tr>
   <td><a href="https://mooc-forums.inria.fr/moocnsi/c/algorithmique/a-propos-du-mooc/200">Bloc 3 du Mooc 1 : Algorithmique</a></td>
</tr>
<tr>
   <td><a href="https://mooc-forums.inria.fr/moocnsi/c/achitecture-materielle-et-systemes-dexploitation/a-propos-du-mooc/203">Bloc 4 du Mooc 1 : Architecture matérielle, OS, réseaux</a></td>
</tr>
<tr>
  <td><a href="https://mooc-forums.inria.fr/moocnsi/c/didactique-de-linformatique/partage-de-pratique/204">Mooc 2 : Partage de pratique</a></td>
  <td colspan="2">On y partage à la fois nos pratiques pédagogiques (ex: comment gérer une classe entière, quelle place pour l’informatique sans ordinateur) et didactiques (ex: comment apprendre la récursivité, quelle est la bonne métaphore pour décrire TCP/IP, …).</td>
</tr>
<tr>
  <td><a href="https://mooc-forums.inria.fr/moocnsi/c/didactique-de-linformatique/epistemologie-de-l-informatique/205">Mooc 2 : Épistémologie de l'informatique</a></td>
  <td colspan="2">On y prend de la hauteur par rapport à l’informatique qui est à la fois science et technique, nous réfléchissons aux liens avec les autres disciplines, à l’impact de la pensée informatique dans les autres sciences, …</td>
</tr>
<tr>
  <td><a href="https://mooc-forums.inria.fr/moocnsi/c/culture-scientifique-et-technique-du-numerique/pedagogie-de-l-egalite/206">Mooc 2 : Pédagogie de l'égalité</a></td>
  <td colspan="2">On y partage les enjeux et les méthodes pour faire de notre enseignement de l’informatique un vrai enseignement égalitaire pour filles et garçons, quel que soit le milieu d’origine, nous abordons aussi les problèmes d’égalité dans le domaine de l’informatique au delà de notre enseignement.</td>
</tr>
<tr>
  <td><a href="https://mooc-forums.inria.fr/moocnsi/c/enseigner-linformatique/134">Mooc 2 : Devenir et être enseignant en informatique</a></td>
  <td colspan="2">On y discute comment se former, préparer le CAPES, quel statut des enseignats.</td>
</tr>
</table>


## Vos Questions !

### Comment s'inscrire sur le forum ?

En trois clics et quelques informations minimales (Nom, Prénom, Email, Choix d'un mot de passe).

![Vue du bandeau du forum](./assets/banner-forum.png)

Si il y a le moindre souci [contactez-nous](./7_Contact.md).

#### Venir sur le forum via la plateforme FUN

__Attention !__ Si vous êtes inscrits sur la [plateforme FUN](https://www.fun-mooc.fr) car vous suivez des formations en ligne, alors vous allez être connectés directement sur ce forum, mais si vous créé un compte directement aussi, il y aura un doublon.

### Qui anime ce forum ?

Ce sont les collègues de l'[Association des enseignantes et enseignants d'informatique de France](http://aeif.fr) avec toute l'équipe que nous formons et le [Learning-Lab Inria](https://learninglab.inria.fr)  (Institut national de recherche en sciences et technologies du numérique), soutenus dans leur intention par l’Inspection Générale, offrent à la très active communauté gravitant autour de l’enseignement NSI (professeur·e·s des lycées, cadre de l’éducation nationale et collègues de l’enseignement supérieur et de la recherche en soutien).

### Quelle complémentarité avec la liste mail NSI ?

C'est grâce à liste de discussion nationale pour les Enseignants de la Spécialité Numérique & Sciences Informatiques de 1ère et Terminale, initiée et portée par Rodrigo Schwencke, que s'est formée une vraie communauté et, nous (les personnes qui co-animons le forum et les ressources proposées sur ce portail) restons abonnées à cette liste pour faire le lien.

Nous sommes passé·e·s à un mécanisme de forum, pour juste mieux structurer les échanges, pérenniser les ressources partagées etc. mais nous continuons de bien travailler en collaboration, mutualiser les outils, etc.

### Quel outil a été utilisé et pourquoi ?

Le forum est basé sur le logiciel libre [Discourse](https://www.discourse.org), porté par le [Learning-Lab Inria](https://learninglab.inria.fr), qui le déploie.

### A-t-on une évaluation de son usage ?

Oui, fin avril 2021, Il y avait plus de 880 utilisateurs enregistrés, avec une engagement quotidien des utilisateurs, quelques dizaines par jour, et près de 850 posts, plus de détails [ici](https://cai.community/breve/en-direct-du-forum-nsi).

<script src="./assets/make_foldable.js"/>
