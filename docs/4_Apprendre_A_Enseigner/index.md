---
title: Présentation
description: "Présentation du MOOC : Apprendre à enseigner le Numérique et les Sciences Informatiques"
---

## La formation : Apprendre à enseigner le Numérique et les Sciences Informatiques

[![](../assets/vignette-NSI2-sc-600px.png)](https://www.fun-mooc.fr/fr/cours/apprendre-a-enseigner-le-numerique-et-les-sciences-informatiques/)

<b style="display:block;float:right">[S'inscrire au MOOC](https://www.fun-mooc.fr/fr/cours/apprendre-a-enseigner-le-numerique-et-les-sciences-informatiques/)</b>

### Objectifs pédagogiques

L'objectif est de permettre à des enseignant·e·s ou en passe de l'être d'apprendre à enseigner l'informatique au niveau du secondaire, donc de :

- Connaître les attendus en matière de compétences des élèves : savoir, savoir-faire et savoir-être,
    - à travers les programmes établis.
- Se familiariser avec les outils logiciels et organisationnels spécifiques de cet enseignement,
    - pouvoir manipuler une plateforme d'apprentissage de la programmation pour une classe,
    - utiliser des outils numériques d'apprentissage de concepts et de pratiques de l'informatique,
    - pouvoir organiser du travail en pédagogie inversée (invitation à découvrir le sujet, puis validation et approfondissement en cours)
    - savoir guider des progressions d'élèves en apprentissage semi-autonome,
    - etc.
- Se mettre en situation d'enseignement : 
    - pouvoir préparer des cours d'informatique théoriques et pratiques,
    - organiser ces cours au sein d'une progression, 
    - mettre en action l'enseignement dans la classe de la mise en activité à l'accompagnement des élèves, 
    - évaluation des acquis et
    - auto-évaluation et amélioration du cours.
- Apprendre par la pratique à créer les ressources dont on a besoin pour ces cours,
    - identifier et évaluer les supports disponibles (ouvrages, fiches, etc...),
    - adapter ces contenus à la situation pédagogique de la classe et à la démarche d'enseignement choisie,
    - thésauriser et reviser les contenus pour un partage collégial et une utilisation ultérieure.

### Format de la formation

Sous forme de cMOOC (c'est à dire un MOOC dit ``communautaire´´ où nous apprenons en créant les contenus ensemble de manière accompganée dont on a besoin) nous offrons un parcours à triple entrée

1. par l’exemple, basé sur des échanges et réalisations en groupe et des témoignages : on prend quelques activités typiques et regarde ensemble toute la démarche 
2. par les thématiques du programme : on co-construct des ressources au fil de l’année pour préparer les cours et évaluer leur réalisation, et apprendre à créer d'autres ressources
3. par une réflexion et un travail sur des thèmes transversaux (ex: travail en projet de groupes d'élèves, apprentissage en autonomie de la programmation, enjeux d'égalité des genres, information sur les débouchés de cet enseignement, pédagogie différenciée pour des élèves à particularité) 

### Prérequis pour l'apprenant·e

Avoir suivi et validé la [formation aux fondamentaux](./3_Les_Fondamentaux.html).

### Compétences acquises et évaluation

Les compétences actives sont celles exprimées dans les objectifs pédagogiques.

L'évaluation de ces compétences se fera de manière formelle par l'évaluation de productions numériques par les pairs et en auto-évaluation par rapport à sa pratique.

_Note :_ Aux personnes en formation qui ne sont pas en situation d'enseignement, il est proposé des "simulations" pour tester les pratiques sur des publics tests.

<script src="../assets/make_foldable.js"/>
